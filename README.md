# Official website of Indian Pirates

You can see the live website at https://pirates.org.in. It is built using a
static site generator tool called hugo. For more details, check
https://gohugo.io.


## Installing Hugo

Hugo is available on most modern operating systems. If you're using a GNU/Linux distribution that uses the `apt` package manager (Debian, Ubuntu or any of their derivatives) you can install it with the following command:

``` sh
sudo apt install hugo
```

If you're using a GNU/Linux distribution that uses the `pacman` package manager (Archlinux or any of it's derivatives) you can use the following command:

``` sh
sudo pacman -S hugo
```

If you're using a Distro that uses the dnf package manger (Fedora) you can use the following command:

``` sh
sudo dnf install hugo
```

## Running a development server

To run a development server run the following command:

``` sh
hugo server -D
```

If you want your server to show your draft posts to run:

``` sh
hugo server -D --buildDrafts
```

## Production setup

The website is updated using a post-receive git hook that automatically builds
the website. The following blog post is used as a reference for the setup:

https://jackatkinson.net/post/git_hooks/


## Deploying updates to server

Note: You need ssh access to the pirates.org.in server to do the following.

``` sh
# Replace <username> and <hostname> with actual values
git remote add piratesin ssh://<username>@<hostname>/~/piratesite-hugo.git
git push piratesin master
